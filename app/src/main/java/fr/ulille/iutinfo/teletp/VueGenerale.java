package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;

public class VueGenerale extends Fragment {

    private String salle;
    private String DISTANCIEL;
    private String poste;
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.DISTANCIEL = this.getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;

        Spinner poste = ((Spinner) getActivity().findViewById(R.id.spPoste));
        Spinner salle = ((Spinner) getActivity().findViewById(R.id.spSalle));
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        ArrayAdapter<CharSequence> posteAdapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_postes, R.layout.support_simple_spinner_dropdown_item);
        poste.setAdapter(posteAdapter);
        ArrayAdapter<CharSequence> salleAdapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_salles, R.layout.support_simple_spinner_dropdown_item);
        salle.setAdapter(salleAdapter);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView tvlog = view.findViewById(R.id.tvLogin);
            model.setUsername(tvlog.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });
       /* poste.setOnItemSelectedListener( (AdapterView.OnItemSelectedListener view2 )->{
          update(); // marche pas :(
        });*/
        this.update();
        // TODO Q9
    }
    public void update(){
        Spinner poste = ((Spinner) getActivity().findViewById(R.id.spPoste));
        Spinner salle = ((Spinner) getActivity().findViewById(R.id.spSalle));
        if(this.salle.equals(this.DISTANCIEL)){
            poste.setVisibility(View.INVISIBLE);
            poste.setEnabled(true);
            model.setLocalisation("Distanciel");
        }else{
            poste.setVisibility(View.VISIBLE);
            poste.setEnabled(false);
            model.setLocalisation(salle.getSelectedItem().toString() + " : " + poste.getSelectedItem().toString());
        }
    }
    // TODO Q9
}