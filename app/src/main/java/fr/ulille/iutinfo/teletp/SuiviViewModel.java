package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private MutableLiveData<String []> liveQuestions;
    private MutableLiveData<Integer> liveNextQuestion;
    private String [] questions;

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = new MutableLiveData<>();
        liveQuestions = new MutableLiveData<>();
        liveNextQuestion.setValue(0);
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    public void initQuestions(Context context){
        this.questions = context.getResources().getStringArray(R.array.list_questions);
        this.liveQuestions.setValue(questions);
    }
    public String getQuestions(int pos){
        return this.questions[pos];
    }
    LiveData<Integer> getLiveNextQuestion(){
        return this.liveNextQuestion;
    }
    public void setNextQuestion(Integer nextQuestion){
        this.liveNextQuestion.setValue(nextQuestion);
    }
    public Integer getNextQuestion(){
        return this.liveNextQuestion.getValue();
    }
}
